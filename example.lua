--[[
	recipe - example.lua
	
	Copyright (c) 2023 hexaheximal

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
]]--

metadata = {
	name="example",
	version="1.0.0",
	architecture_specific=false,
	libc_specific=false,
	dependencies={},
	breaks={},
	replaces={},
	flags={"test", "test2"},
	default_flags={"test"},
	files={
		{"index.html", "https://example.com/index.html"}
	}
}

function build()
	print("----- build info -----")
	print("cc: " .. toolchain("cc"))
	print("c++: " .. toolchain("c++"))
	print("go: " .. toolchain("go"))
	print("build path: " .. build_path)
	print("target path: " .. target_path)
	print("target release: " .. release)
	print("target arch: " .. arch)
	print("target libc: " .. libc)
	print("target flavor: " .. flavor)
	print("test: " .. tostring(isEnabled("test")))
	print("test2: " .. tostring(isEnabled("test2")))
	print("----------------------")

	print("copying index.html...")

	run("mkdir", {"-p", target_path .. "/etc/example"})
	run("cp", {build_path .. "/index.html", target_path .. "/etc/example/index.html"})

	print("done!")
end

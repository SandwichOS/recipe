/*
	recipe - util/downloader.go

	Copyright (c) 2023 hexaheximal

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
*/

package util

import (
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

type Downloader struct {
	CachePath string
}

func (d *Downloader) GetCachePath(url string) string {
	return filepath.Join(d.CachePath, strings.ReplaceAll(url, "://", "/"))
}

// Fetches a file from a given URL.

func (d *Downloader) Fetch(url string) (io.ReadCloser, error) {
	if !DirectoryExists(d.CachePath) {
		log.Println("downloader [WARN]: Cache directory does not exist. Downloading directly.")
		return d.RawFetch(url)
	}

	path := d.GetCachePath(url)

	if FileExists(path) {
		log.Println("downloader [INFO]: Fetching", url, "from the cache...")

		f, err := os.Open(path)

		if err != nil {
			return nil, err
		}

		return f, nil
	}

	return d.RawFetch(url)
}

// Same as Fetch(), but it bypasses the cache.

func (d *Downloader) RawFetch(url string) (io.ReadCloser, error) {
	log.Println("downloader [INFO]: Fetching", url, "from the network...")

	response, err := http.Get(url)

	if err != nil {
		return nil, err
	}

	if !DirectoryExists(d.CachePath) {
		log.Println("downloader [WARN]: Cache directory does not exist. Skipping all cache functionality and returning the raw download stream.")
		return response.Body, nil
	}

	path := d.GetCachePath(url)

	err = os.MkdirAll(filepath.Dir(path), os.ModePerm)

	if err != nil {
		return nil, err
	}

	f, err := os.Create(path)

	if err != nil {
		return nil, err
	}
	_, err = io.Copy(f, response.Body)

	if err != nil {
		return nil, err
	}

	response.Body.Close()

	return f, nil
}

func (d *Downloader) FileFetch(url string, target string) error {
	response, err := d.Fetch(url)

	if err != nil {
		panic(err)
	}

	defer response.Close()

	// Write the data to the target file

	f, err := os.Create(target)

	if err != nil {
		panic(err)
	}

	defer f.Close()

	_, err = io.Copy(f, response)

	if err != nil {
		return nil
	}

	f.Sync()

	return nil
}

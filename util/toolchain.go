/*
	recipe - util/toolchain.go

	Copyright (c) 2023 hexaheximal

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
*/

package util

import (
	"errors"
	"os"
	"os/exec"
	"path/filepath"
)

func CommandExists(name string) bool {
	cmd := exec.Command("/bin/sh", "-c", "command -v "+name)
	err := cmd.Run()
	return err == nil
}

func GetHostToolchain(name string) (string, error) {
	switch name {
	case "cc":
		if os.Getenv("CC") != "" && CommandExists(os.Getenv("CC")) {
			return os.Getenv("CC"), nil
		}

		if os.Getenv("FORCE_CLANG") != "true" {
			if CommandExists("gcc") {
				return "gcc", nil
			}
		}

		if CommandExists("clang") {
			return "clang", nil
		}

		return "", errors.New("failed to find gcc or clang, and $CC is not set to a valid command")
	case "c++":
		if os.Getenv("CC") != "" && CommandExists(os.Getenv("CC")) {
			return os.Getenv("CC"), nil
		}

		if os.Getenv("FORCE_CLANG") != "true" {
			if CommandExists("g++") {
				return "g++", nil
			}
		}

		if CommandExists("clang++") {
			return "clang++", nil
		}

		return "", errors.New("failed to find g++ or clang++, and $CXX is not set to a valid command")
	case "go":
		if CommandExists("go") {
			return "go", nil
		}

		if os.Getenv("GOROOT") != "" && FileExists(filepath.Join(os.Getenv("GOROOT"), "bin/go")) {
			return filepath.Join(os.Getenv("GOROOT"), "bin/go"), nil
		}

		return "", errors.New("failed to find go, and $GOROOT is not set to a valid go installation")
	}

	return "", errors.New("unknown toolchain name")
}

/*
	recipe - util/table.go

	Copyright (c) 2023 hexaheximal

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
*/

package util

import (
	"errors"

	lua "github.com/yuin/gopher-lua"
)

func TableToMap(table *lua.LTable) (map[string]interface{}, error) {
	output := make(map[string]interface{})

	var err error = nil

	table.ForEach(func(key lua.LValue, value lua.LValue) {
		keyString := (key.(lua.LString)).String()

		switch value.Type() {
		case lua.LTString:
			output[keyString] = (value.(lua.LString)).String()
		case lua.LTBool:
			output[keyString] = value.(lua.LBool)
		case lua.LTTable:
			if IsTableArray(value.(*lua.LTable)) {
				innerSlice, innerErr := TableToSlice(value.(*lua.LTable))

				err = innerErr

				output[keyString] = innerSlice

				break
			}

			innerMap, innerErr := TableToMap(value.(*lua.LTable))

			err = innerErr

			output[keyString] = innerMap
		default:
			err = errors.New("unknown type: " + value.Type().String())
		}
	})

	return output, err
}

func IsTableArray(table *lua.LTable) bool {
	isArray := true

	table.ForEach(func(key lua.LValue, value lua.LValue) {
		if !isArray {
			return
		}

		if key.Type() != lua.LTNumber {
			isArray = false
		}
	})

	return isArray
}

func TableToSlice(table *lua.LTable) ([]interface{}, error) {
	output := make([]interface{}, 0)

	var err error = nil

	table.ForEach(func(key lua.LValue, value lua.LValue) {
		switch value.Type() {
		case lua.LTString:
			output = append(output, value.(lua.LString))
		case lua.LTBool:
			output = append(output, value.(lua.LBool))
		case lua.LTTable:
			if IsTableArray(value.(*lua.LTable)) {
				innerSlice, innerErr := TableToSlice(value.(*lua.LTable))

				err = innerErr

				output = append(output, innerSlice)

				break
			}

			innerMap, innerErr := TableToMap(value.(*lua.LTable))

			err = innerErr

			output = append(output, innerMap)
		default:
			err = errors.New("unknown type: " + value.Type().String())
		}
	})

	return output, err
}

func InterfaceSliceToStringSlice(input []interface{}) ([]string, error) {
	output := make([]string, len(input))

	for index, value := range input {
		stringValue, ok := value.(lua.LString)

		if !ok {
			return output, errors.New("got a non-string value")
		}

		output[index] = stringValue.String()
	}

	return output, nil
}

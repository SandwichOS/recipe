/*
	recipe - main.go

	Copyright (c) 2023 hexaheximal

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
*/

package main

import (
	"fmt"
	"os"
	"path/filepath"
	"recipe/build"
	"recipe/ingredient"
	"recipe/util"
)

func main() {
	if 2 > len(os.Args) {
		fmt.Println("Usage: recipe {build,info}")
		os.Exit(1)
	}

	cwd, err := os.Getwd()

	if err != nil {
		panic(err)
	}

	workdir := filepath.Join(cwd, ".recipe")

	if os.Getenv("RECIPE_WORKDIR") != "" {
		workdir = os.Getenv("RECIPE_WORKDIR")
	}

	if !util.DirectoryExists(workdir) {
		err = os.MkdirAll(workdir, os.ModePerm)

		if err != nil {
			panic(err)
		}
	}

	switch os.Args[1] {
	case "build":
		if 3 > len(os.Args) {
			fmt.Println("Usage: recipe build [filename]")
			os.Exit(1)
		}

		data, err := os.ReadFile(os.Args[2])

		if err != nil {
			panic(err)
		}

		err = build.Build(string(data), workdir, build.BuildTarget{
			Release:      "rolling",
			Architecture: "host",
			Libc:         "host",
			Flavor:       "host",
		})

		if err != nil {
			panic(err)
		}
	case "info":
		if 7 > len(os.Args) {
			fmt.Println("Usage: recipe info [name] [release] [arch] [libc] [flavor]")
			os.Exit(1)
		}

		f, err := os.Open(filepath.Join(workdir, "ingredients", os.Args[3], os.Args[4], os.Args[5], os.Args[6], os.Args[2]+".ing"))

		if os.IsNotExist(err) {
			fmt.Printf("FATAL: Failed to find \"%s\", make sure that it exists and try again.\n", os.Args[2])
			os.Exit(1)
		}

		if err != nil {
			panic(err)
		}

		i, compressionType, err := ingredient.ReadHeader(f)

		if err != nil {
			panic(err)
		}

		fmt.Println("Name:", i.Name)
		fmt.Println("Version:", i.Version)
		fmt.Println("Release:", i.Release)
		fmt.Println("Architecture:", i.Architecture)
		fmt.Println("Flavor:", i.Flavor)
		fmt.Println("Libc:", i.Libc)
		fmt.Println("Dependencies:", i.Dependencies)
		fmt.Println("Breaks:", i.Breaks)
		fmt.Println("Replaces:", i.Replaces)
		fmt.Println("Compression type:", compressionType)
	default:
		fmt.Println("Invalid subcommand:", os.Args[1])
		os.Exit(1)
	}
}

# The Specification

⚠️ **WARNING: This is not yet production-ready.** ⚠️

This document describes the entire design of recipe, as well as the design decisions that were made along the way.

## The sandwich metaphor

The idea is that the entire system is a software sandwich containing multiple packages that are called ingredients in SandwichOS.

Recipes can either be partial (basically just a group of packages to install on top of the system) or complete. (which contains the full list of packages for the system)

## History

SandwichOS was originally created as a joke on June 7, 2022. However, there were many issues with the original implementation:

1. The original package manager was slice. The name slice doesn't fit well with the new sandwich metaphor.
2. It stored a file named `metadata.json` with the metadata in a way where searching through the tarball for the metadata file is O(n), when it really only needs to be O(1).
3. It used gzip compression on the tarballs in a way that required it to be entirely decompressed into memory whenever a package is installed, which does not scale well.

But most importantly...

It didn't even have a proper way to build packages. They were built with shell scripts instead.

## Ingredients

Ingredients (known outside of the SandwichOS project as packages) are handled an unusual (but really useful) way in SandwichOS where you can choose to either use binary packages or compile them from source. It's close to what Gentoo does, but not quite.

One major thing to note is that ingredients MUST be buildable offline by pre-downloading the source code.

For modern languages which require downloading dependencies, this is done via a proxy.

## Host Ingredients

Host Ingredients are almost the same as normal ingredients, with the exception of it using the host toolchain and targeting the host machine.

For example, a gcc host ingredient would use the host system's gcc to bootstrap a compiler which would then be used for building the target system.

# Overlays

Overlays are an important concept in recipe.

The idea came out of frustration with how large package repositories are for most linux distributions (most notably Debian), as well as how Debian forces stable versions for *all* packages and doesn't support including some rolling-release packages for specific things (most notably drivers and programming languages) which aren't designed to work with Debian's stability model.

The idea of overlays is simple: instead of creating one large package repository, we create a lot of smaller ones with their own category.

Overlays solve the first problem quite by design since it's more modular, and it solves the second problem by allowing multiple overlays to exist, one for the stable version and one for the rolling-release version.

## Ingredient binary package files

The file format for ingredient binary packages is defined as the following:

Size (bytes) | Description
|:--|:--
4 | Magic bytes, always F0 9F A5 AA EB EA 86 07
1 | Format version, latest version is 0x01
1 | Compression type - see the next section for more info
4 | Metadata size, encoded as a uint32
?? | Serialized metadata, see section below
?? | Package data as a tarball

## Compression types

Value | Description
|:--|:--
0x00 | No compression.
0x01 | gzip
0x02 | zstd
0x03 | xz

## Ingredient metadata

The metadata of an ingredient includes the following:

- The name of the ingredient
- The version of the ingredient
- Which release it is for (or `rolling` for the rolling-release version)
- The ingredient architecture (or `any` if it doesn't depend on a specific architecture)
- The architecture flavor (or `any` if it works on all flavors with no differences)
- The libc it is built for (or `any` if it doesn't need a specific libc)
- The dependencies it has
- Which ingredients it breaks
- Which ingredients it replaces

Here are the rules for what the metadata values are:

- All values MUST NOT be blank, except for the dependencies, the ingredients it breaks, and the ingredients it replaces
- The ingredient name MUST only contain lowercase letters, numbers, and/or dashes
- The ingredient version MUST NOT start with "v" (for example, "1.0.0" is valid but "v1.0.0" is not)
- The release name MUST be a valid release name
- The architecture MUST be `any` if the ingredient does not contain any binaries
- The architecture flavor MUST be `any` if the architecture is `any`
- The architecture flavor MUST NOT be `any` if the architecture is not `any`
- The architecture flavor MUST be a gcc march value if it is not `any`. For x86(_x64), see https://gcc.gnu.org/onlinedocs/gcc-13.1.0/gcc/x86-Options.html
- The libc name MUST be `any` if the architecture is `any`
- The libc name MUST be `any` if the ingredient contains binaries that do not require a specific libc (e.g programs written in Go that don't use cgo)

The ingredient metadata is serialized as json.

Here is an example:

```json
{
	"name": "example",
	"version": "0.0.1",
	"release": "rolling",
	"architecture": "amd64",
	"flavor": "x86-64",
	"libc": "glibc",
	"dependencies": ["dependency1", "dependency2"],
	"breaks": [],
	"replaces": []
}
```

## Ingredient Flags

Ingredient flags are used to configure ingredients.

The rules for flags are the following:

- Flags are separated with spaces
- Flag names MUST only contain lowercase letters, numbers, and/or dashes (except for the wildcard flag `*`)
- `*` can be used to add all optional flags (and then the flags that come after it can remove them), but it MUST NOT be anything other than the first flag specified
- `-*` can be used to remove all optional flags (and then the flags that come after it can add them back), but it MUST NOT be anything other than the first flag specified
- `-[flag name]` can be used to remove a flag

Here a few examples:

- `a b c` - Adds `a`, `b`, and `c`.
- `-* a` - Removes all flags except for `a`. 
- `* -b` - Includes all flags except for `b`.

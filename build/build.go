/*
	recipe - build/build.go

	Copyright (c) 2023 hexaheximal

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
*/

package build

import (
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"recipe/ingredient"
	"recipe/util"

	lua "github.com/yuin/gopher-lua"
)

type BuildTarget struct {
	Release      string
	Architecture string
	Libc         string
	Flavor       string
}

var enabledFlags []string
var downloader util.Downloader

// Lua -> Go

func Run(L *lua.LState) int {
	command := L.ToString(1)

	slice, err := util.TableToSlice(L.ToTable(2))

	if err != nil {
		L.RaiseError(err.Error())
		return 0
	}

	arguments, err := util.InterfaceSliceToStringSlice(slice)

	if err != nil {
		L.RaiseError(err.Error())
		return 0
	}

	c := exec.Command(command, arguments...)

	c.Stdout = os.Stdout

	err = c.Run()

	if err != nil {
		L.RaiseError(err.Error())
		return 0
	}

	return 0
}

func IsEnabled(L *lua.LState) int {
	flagName := L.ToString(1)

	for _, value := range enabledFlags {
		if value == flagName {
			L.Push(lua.LBool(true))
			return 1
		}
	}

	L.Push(lua.LBool(false))
	return 1
}

func Toolchain(L *lua.LState) int {
	// TODO: target toolchain

	command, err := util.GetHostToolchain(L.ToString(1))

	if err != nil {
		L.RaiseError(err.Error())
		return 0
	}

	L.Push(lua.LString(command))
	return 1
}

func Build(buildScript string, workdir string, target BuildTarget) error {
	log.Println("Preparing...")

	ingredientPath := filepath.Join(workdir, "ingredients", target.Release, target.Architecture, target.Libc, target.Flavor)
	cachePath := filepath.Join(workdir, "cache")
	buildPath := filepath.Join(workdir, "build")
	targetPath := filepath.Join(workdir, "target")

	if !util.DirectoryExists(ingredientPath) {
		err := os.MkdirAll(ingredientPath, os.ModePerm)

		if err != nil {
			return err
		}
	}

	if !util.DirectoryExists(cachePath) {
		err := os.MkdirAll(cachePath, os.ModePerm)

		if err != nil {
			return err
		}
	}

	if util.DirectoryExists(buildPath) {
		err := os.RemoveAll(buildPath)

		if err != nil {
			return err
		}
	}

	err := os.MkdirAll(buildPath, os.ModePerm)

	if err != nil {
		return err
	}

	if util.DirectoryExists(targetPath) {
		err := os.RemoveAll(targetPath)

		if err != nil {
			return err
		}
	}

	err = os.MkdirAll(targetPath, os.ModePerm)

	if err != nil {
		return err
	}

	downloader = util.Downloader{
		CachePath: cachePath,
	}

	L := lua.NewState()

	defer L.Close()

	L.SetGlobal("run", L.NewFunction(Run))
	L.SetGlobal("isEnabled", L.NewFunction(IsEnabled))
	L.SetGlobal("toolchain", L.NewFunction(Toolchain))

	L.SetGlobal("build_path", lua.LString(buildPath))
	L.SetGlobal("target_path", lua.LString(targetPath))

	L.SetGlobal("release", lua.LString(target.Release))
	L.SetGlobal("arch", lua.LString(target.Architecture))
	L.SetGlobal("libc", lua.LString(target.Libc))
	L.SetGlobal("flavor", lua.LString(target.Flavor))

	err = L.DoString(buildScript)

	if err != nil {
		return nil
	}

	metadataTable := L.GetGlobal("metadata").(*lua.LTable)

	metadata, err := util.TableToMap(metadataTable)

	if err != nil {
		return err
	}

	dependencies, err := util.InterfaceSliceToStringSlice(metadata["dependencies"].([]interface{}))

	if err != nil {
		return err
	}

	breaks, err := util.InterfaceSliceToStringSlice(metadata["breaks"].([]interface{}))

	if err != nil {
		return err
	}

	replaces, err := util.InterfaceSliceToStringSlice(metadata["replaces"].([]interface{}))

	if err != nil {
		return err
	}

	defaultFlags, err := util.InterfaceSliceToStringSlice(metadata["default_flags"].([]interface{}))

	if err != nil {
		return err
	}

	enabledFlags = defaultFlags

	files := metadata["files"].([]interface{})

	if err != nil {
		return err
	}

	for _, value := range files {
		slice, err := util.InterfaceSliceToStringSlice(value.([]interface{}))

		if err != nil {
			return err
		}

		downloader.FileFetch(slice[1], filepath.Join(buildPath, slice[0]))
	}

	log.Println("Building...")

	err = L.CallByParam(lua.P{
		Fn:   L.GetGlobal("build"),
		NRet: 1, Protect: true,
	}, lua.LString(buildPath))

	if err != nil {
		return err
	}

	i := ingredient.Ingredient{
		Name:         metadata["name"].(string),
		Version:      metadata["version"].(string),
		Release:      target.Release,
		Architecture: target.Architecture,
		Flavor:       target.Flavor,
		Libc:         target.Libc,
		Dependencies: dependencies,
		Breaks:       breaks,
		Replaces:     replaces,
	}

	log.Println("Writing ingredient file...")

	f, err := os.Create(filepath.Join(ingredientPath, i.Name+".ing"))

	if err != nil {
		return err
	}

	err = i.WriteHeader(f, ingredient.COMPRESSION_NONE)

	if err != nil {
		return err
	}

	err = util.WriteTarball(f, targetPath)

	if err != nil {
		return err
	}

	log.Println("Cleaning up build environment...")

	err = os.RemoveAll(targetPath)

	if err != nil {
		return err
	}

	err = os.RemoveAll(buildPath)

	if err != nil {
		return err
	}

	log.Println("Finished successfully!")

	return nil
}

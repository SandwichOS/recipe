/*
	recipe - ingredient/ingredient.go

	Copyright (c) 2023 hexaheximal

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
*/

package ingredient

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"errors"
	"io"
)

const (
	COMPRESSION_NONE byte = 0x00
)

type Ingredient struct {
	Name         string   `json:"name"`
	Version      string   `json:"version"`
	Release      string   `json:"release"`
	Architecture string   `json:"architecture"`
	Flavor       string   `json:"flavor"`
	Libc         string   `json:"libc"`
	Dependencies []string `json:"dependencies"`
	Breaks       []string `json:"breaks"`
	Replaces     []string `json:"replaces"`
}

// Check the validity of the metadata

func (i Ingredient) CheckValidity() bool {
	// All values MUST NOT be blank, except for the dependencies, the ingredients it breaks, and the ingredients it replaces

	if i.Name == "" || i.Version == "" || i.Release == "" || i.Architecture == "" || i.Flavor == "" || i.Libc == "" {
		return false
	}

	// TODO: The ingredient name MUST only contain lowercase letters, numbers, and/or dashes

	// The ingredient version MUST NOT start with "v" (for example, "1.0.0" is valid but "v1.0.0" is not)

	if i.Version[0] == 'v' {
		return false
	}

	// The release name MUST be a valid release name

	if i.Release != "rolling" {
		return false
	}

	// Note: We can't enforce "The architecture MUST be any if the ingredient does not contain any binaries" here as this function does not have access to the ingredient files.

	// The architecture flavor MUST be any if the architecture is any

	if i.Architecture == "any" && i.Flavor != "any" {
		return false
	}

	// The architecture flavor MUST NOT be any if the architecture is not any

	if i.Flavor == "any" && i.Architecture != "any" {
		return false
	}

	// TODO: The architecture flavor MUST be a gcc march value if it is not any.

	// The libc name MUST be any if the architecture is any

	if i.Architecture == "any" && i.Libc != "any" {
		return false
	}

	// Note: We can't enforce "The libc name MUST be any if the ingredient contains binaries that do not require a specific libc" here as this function does not have access to the ingredient files.

	// If all checks pass, return true

	return true
}

func (i Ingredient) WriteHeader(w io.Writer, compressionType byte) error {
	// Write the magic bytes

	_, err := w.Write([]byte{0xf0, 0x9f, 0xa5, 0xaa, 0xeb, 0xea, 0x86, 0x07})

	if err != nil {
		return err
	}

	// Write the format version

	_, err = w.Write([]byte{0x01})

	if err != nil {
		return err
	}

	// Write the compression type

	_, err = w.Write([]byte{compressionType})

	if err != nil {
		return err
	}

	// Serialize the metadata

	metadata, err := json.Marshal(i)

	if err != nil {
		return err
	}

	// Write the metadata length

	buffer := make([]byte, 4)

	binary.BigEndian.PutUint32(buffer, uint32(len(metadata)))

	_, err = w.Write(buffer)

	if err != nil {
		return err
	}

	// Write the metadata

	_, err = w.Write(metadata)

	if err != nil {
		return err
	}

	return nil
}

func ReadHeader(r io.Reader) (Ingredient, byte, error) {
	var i Ingredient

	// Read the first few bytes so we can check if it's a valid magic value

	buffer := make([]byte, 8)
	n, err := r.Read(buffer)

	if err != nil {
		return i, COMPRESSION_NONE, err
	}

	if n != 8 || !bytes.Equal(buffer, []byte{0xf0, 0x9f, 0xa5, 0xaa, 0xeb, 0xea, 0x86, 0x07}) {
		return i, COMPRESSION_NONE, errors.New("invalid ingredient magic bytes")
	}

	// Read the format version

	buffer = make([]byte, 1)
	n, err = r.Read(buffer)

	if err != nil {
		return i, COMPRESSION_NONE, err
	}

	if n != 1 {
		return i, COMPRESSION_NONE, errors.New("reached the end of the file while trying to read the format version")
	}

	if buffer[0] != 0x01 {
		return i, COMPRESSION_NONE, errors.New("invalid format version")
	}

	// Read the rest of the header

	buffer = make([]byte, 5)
	n, err = r.Read(buffer)

	if err != nil {
		return i, COMPRESSION_NONE, err
	}

	if n != 5 {
		return i, COMPRESSION_NONE, errors.New("reached the end of the file while trying to read the header")
	}

	if buffer[0] != COMPRESSION_NONE {
		return i, COMPRESSION_NONE, errors.New("invalid compression type")
	}

	compressionType := buffer[0]
	metadataSize := int(binary.BigEndian.Uint32(buffer[1:5]))

	// Read the metadata

	buffer = make([]byte, metadataSize)
	n, err = r.Read(buffer)

	if err != nil {
		return i, COMPRESSION_NONE, err
	}

	if n != metadataSize {
		return i, COMPRESSION_NONE, errors.New("reached the end of the file while trying to read the metadata")
	}

	err = json.Unmarshal(buffer, &i)

	if err != nil {
		return i, COMPRESSION_NONE, err
	}

	return i, compressionType, nil
}
